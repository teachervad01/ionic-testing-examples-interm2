import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";
import { HttpClientModule } from "@angular/common/http";
import { RouterTestingModule } from "@angular/router/testing";
import { DetailsPage } from "./../details/details.page";
import { HomePage } from "./home.page";
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";

describe("HomePage", () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let debugEl: DebugElement;
  let htmlEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage, DetailsPage],
      imports: [
        IonicModule.forRoot(),
        HttpClientModule,
        RouterTestingModule.withRoutes([
          { path: "details", component: DetailsPage },
        ]),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => {
    fixture.destroy();
    component = null;
    debugEl = null;
    htmlEl = null;
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should display news having at least title and description ", () => {
    const items: any = {
      articles: [
        {
          author: "Test Author 1",
          publishedAt: "2020-04-14T01:46:39Z",
          urlToImage: "Test1.jpg",
          title: "Test Title 1",
          content: "Test content 1",
          description: "Test description 1",
        },
        {
          author: "Test Author 2",
          publishedAt: "2020-04-14T02:46:39Z",
          urlToImage: "Test2.jpg",
          title: "Test Title 2",
          content: "Test content 2",
          description: "Test description 2",
        },
        {
          author: "Test Author 3",
          publishedAt: "2020-04-14T03:46:39Z",
          urlToImage: "Test3.jpg",
          title: "Test Title 3",
          content: "Test content 3",
          description: "Test description 3",
        },
      ],
    };
    const firstNews = items.articles[0];
    component.items = items;
    fixture.detectChanges();
    debugEl = fixture.debugElement.query(By.css("ion-card-content"));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain(firstNews.title);
    expect(htmlEl.textContent).toContain(firstNews.description);
    //you can continue with the other elements
  });

  it("should demonstrate some funny test specification", () => {
    expect(2 * 2).toBe(4);
  });
});
